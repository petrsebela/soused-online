<?php
declare(strict_types = 1);

namespace Model\Enum;

class CourierLogEnum extends BaseEnum
{
    const TYPE_ACCIDENT = "ACCIDENT";  //NEHODA

    public static function getConstants(): array
    {
        $reflectionClass = new \ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    public static function getEnum(): array
    {
        $enum = [];
        foreach (static::getConstants() as $const => $value)
        {
            $enum[$value] = $value;
        }
        return $enum;
    }
}