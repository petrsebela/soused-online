<?php
declare(strict_types = 1);

namespace Model\Enum;

class VehicleLogEnum extends BaseEnum
{
    const VEHICLE_LOG_ACCIDENT = "ACCIDENT";  //NEHODA
    const VEHICLE_LOG_TANKING = "TANKING";  //TANKOVÁNÍ
    const VEHICLE_LOG_ASSIGN = "ASSIGN";    //PŘEVZETÍ
    const VEHICLE_LOG_DESSIGN = "DESSIGN";    //PŘEDÁNÍ


    public static function getConstants(): array
    {
        $reflectionClass = new \ReflectionClass(static::class);
        return $reflectionClass->getConstants();
    }

    public static function getEnum(): array
    {
        $enum = [];
        foreach (static::getConstants() as $const => $value)
        {
            $enum[$value] = $value;
        }
        return $enum;
    }
}