<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Zip
 * @package App\Model
 * @property int $id {primary}
 * @property int $zip
 * @property HasOne|Action $city {m:1 City::$zips}
 * @property HasMany|Order[] $orders {1:m Order::$zip}
 * @property HasMany|Order[] $ordersInvoicing {1:m Order::$invoicingZip}
 * @property HasMany|PartnerBranch[] $partnerBranches {1:m PartnerBranch::$zip}
 * @property boolean $deleted {default false}
 */
class Zip extends Entity
{

}