<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Document
 * @package App\Model
 * @property int $id {primary}
 * @property string|NULL $name
 * @property string|NULL $filePath
 * @property \DateTimeImmutable $createdAt {default now}
 * @property HasOne|Partner|NULL $partner {m:1 Partner::$documents}
 */
class Document extends Entity
{

}