<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class PartnersRepository extends Repository
{
	static function getEntityClassNames(): array{
		return [Partner::class];
	}
}