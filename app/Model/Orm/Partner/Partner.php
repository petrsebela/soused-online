<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;

/**
 * Partner
 * @property int $id {primary}
 * @property string $name
 * @property \DateTimeImmutable $partnerFrom
 * @property string $email
 * @property string $phoneNumber
 * @property string $ico
 * @property string $dic
 * @property \DateTimeImmutable $createdAt
 * @property User|NULL $adminUser {1:1 User::$partner, isMain = TRUE}
 * @property HasMany|PartnerBranch[]|NULL $branches {1:m PartnerBranch::$partner}
 * @property HasMany|Document[]|NULL $documents {1:m Document::$partner}
 * @property bool $deleted {default false}
 */
class Partner extends Entity
{

}
