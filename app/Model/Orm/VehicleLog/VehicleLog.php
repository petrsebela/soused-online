<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Model\Enum\VehicleLogEnum;

/**
 * Class VehicleLog
 * @package App\Model
 * @property int $id {primary}
 * @property string $event {enum VehicleLogEnum::VEHICLE_LOG_*}
 * @property \DateTimeImmutable $createdAt {default now}
 * @property User $createdBy {m:1 User::$vehicleLogs}
 * @property Vehicle $vehicle {m:1 Vehicle::$vehicleLogs}
 */
class VehicleLog extends Entity
{

}