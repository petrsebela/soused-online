<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Mapper\Mapper;

abstract class BaseMapper extends Mapper
{
}