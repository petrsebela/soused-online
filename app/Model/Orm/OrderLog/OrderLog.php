<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class OrderLog
 * @package App\Model
 * @property int $id {primary}
 * @property HasOne|Order $order {m:1 Order::$orderLogs}
 * @property HasOne|OrderState $orderState {m:1 OrderState::$orderLogs}
 * @property User|NULL $createdBy {m:1 User::$orderLogs}
 * @property User|NULL $assignedTo {m:1 User::$orderLogAssigned}
 * @property \DateTimeImmutable $createdAt {default now}
 */
class OrderLog extends Entity
{

}