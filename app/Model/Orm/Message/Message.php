<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;

/**
 * Class Message
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum self::TYPE_*}
 * @property User $from
 * @property User $to
 * @property string $text
 * @property \DateTimeImmutable $createdAt {default now}
 */
class Message extends Entity
{
    const TYPE_SUCCESS = "SUCCESS";
    const TYPE_INFO = "INFO";
    const TYPE_DANGER = "DANGER";
    const TYPE_WARNING = "WARNING";
}