<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;

/**
 * Class City
 * @package App\Model
 * @property int $id {primary}
 * @property string $name
 * @property HasMany|Zip[] $zips {1:m Zip::$city}
 * @property HasMany|Order[] $orders {1:m Order::$city}
 * @property HasMany|Order[] $invoicingOrders {1:m Order::$invoicingCity}
 * @property HasMany|PartnerBranch[] $partnerBranches {1:m PartnerBranch::$city}
 */
class City extends Entity
{

}