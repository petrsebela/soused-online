<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;

/**
 * Class Knowledge
 * @package App\Model
 * @property int $id {primary}
 * @property string $heading
 * @property string|NULL $description
 * @property \DateTimeImmutable $createdAt {default now}
 */
class Knowledge extends Entity
{

}