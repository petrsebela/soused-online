<?php

namespace App\Model;

use Nextras\Orm\Repository\Repository;

class PartnerBranchesRepository extends Repository
{
	static function getEntityClassNames(): array{
		return [PartnerBranch::class];
	}

	public function findByPartner(Partner $partner)
    {
        return $this->findBy(['partner' => $partner, 'deleted' => false]);
    }
}