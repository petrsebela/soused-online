<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * PartnerBranch
 * @property int $id {primary}
 * @property string $name
 * @property \DateTimeImmutable $createdAt
 * @property string $email
 * @property string $phoneNumber
 * @property HasOne|Partner $partner {m:1 Partner::$branches}
 * @property string $addressNumber
 * @property string $street
 * @property HasOne|City $city {m:1 City::$partnerBranches}
 * @property HasOne|Zip $zip {m:1 Zip::$partnerBranches}
 * @property User|NULL $branchUser {1:1 User::$branch, isMain = TRUE}
 * @property HasMany|PartnerBranch[] $orders {1:m Order::$partnerBranch}
 * @property bool $deleted {default false}
 */
class PartnerBranch extends Entity
{

}
