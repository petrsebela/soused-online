<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class OrderItem
 * @package App\Model
 * @property int $id {primary}
 * @property HasOne|Order $order {m:1 Order::$orderItems}
 * @property string $itemName
 * @property string|NULL $description
 * @property float|NULL $weight
 * @property string|NULL $resolution
 * @property float $brutto
 * @property float $netto
 * @property int $vat
 * @property HasOne|OrderItemState $orderItemState {m:1 OrderItemState::$orderItems}
 * @property int $quantity
 * @property \DateTimeImmutable $createdAt {default now}
 */
class OrderItem extends Entity
{

}