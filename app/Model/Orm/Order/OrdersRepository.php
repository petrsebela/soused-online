<?php

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Repository\Repository;

/**
 * Class OrdersRepository
 * @package App\Model
 */
class OrdersRepository extends Repository
{


    public function findOrdersForCourier(User $user, string $orderStates)
    {
        return $this->mapper->findOrdersForCourier( $user,  $orderStates);
    }

    public static function getEntityClassNames(): array
    {
        return [Order::class];
    }

    public function getDayTurnover(\DateTimeImmutable $date): float
    {
        $turnover = 0;
        $orders = $this->findBy(['createdAt>=' => $date->setTime(0,0,0), 'createdAt<=' => $date->setTime(23,59,59)]);
        /** @var Order $order */
        foreach ($orders as $order)
        {
            foreach ($order->orderItems as $orderItem)
            {
                $turnover += $orderItem->brutto * $orderItem->quantity;
            }
        }
        return $turnover;
    }

    public function getDayOrdersSold(\DateTimeImmutable $date): int
    {
        return $this->findBy(['createdAt>=' => $date->setTime(0,0,0), 'createdAt<=' => $date->setTime(23,59,59)])->count();
    }

    public function getDayProductsSold(\DateTimeImmutable $date): int
    {
        $quantity = 0;
        $orders = $this->findBy(['createdAt>=' => $date->setTime(0,0,0), 'createdAt<=' => $date->setTime(23,59,59)]);
        /** @var Order $order */
        foreach ($orders as $order)
        {
            foreach ($order->orderItems as $orderItem)
            {
                $quantity += $orderItem->quantity;
            }
        }
        return $quantity;
    }

    public function getLastOrderLog(Order $order): ?OrderLog
    {
        foreach ($order->orderLogs->toCollection()->orderBy('createdAt', 'DESC')->limitBy(1) as $orderLog)
        {
            return $orderLog;
        }
        return NULL;
    }

}