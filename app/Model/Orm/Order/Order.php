<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class Order
 * @package App\Model
 * @property int $id {primary}
 * @property string $eanCode
 * @property \DateTimeImmutable $createdAt {default now}
 * @property string $contactName
 * @property string $contactSurname
 * @property string $addressNumber
 * @property string $street
 * @property HasOne|City $city {m:1 City::$orders}
 * @property HasOne|Zip $zip {m:1 Zip::$orders}
 * @property string $phoneNumber
 * @property string $email
 * @property string $nameInvoicing
 * @property string $surnameInvoicing
 * @property string $streetInvoicing
 * @property HasOne|City $invoicingCity {m:1 City::$invoicingOrders}
 * @property HasOne|Zip $invoicingZip {m:1 Zip::$ordersInvoicing}
 * @property \DateTimeImmutable $deliveryDate
 * @property boolean $termsOfServicesAgreement
 * @property string $orderSource
 * @property string|NULL $ic
 * @property string|NULL $dic
 * @property HasOne|PartnerBranch|NULL $partnerBranch {m:1 PartnerBranch::$orders}
 * @property HasMany|OrderItem[]|NULL $orderItems {1:m OrderItem::$order}
 * @property HasMany|OrderLog[] $orderLogs {1:m OrderLog::$order}
 * @property HasOne|DeliveryType $deliveryType {m:1 DeliveryType::$orders}
 * @property HasOne|PaymentType $paymentType {m:1 PaymentType::$orders}
 */
class Order extends Entity
{

    public function getAddressString(): string
    {
        return $this->street . ' ' . $this->addressNumber . ', ' . $this->city->name . ' ' . $this->zip->zip;
    }

    public function getLastOrderLog(): ?OrderLog
    {
        foreach ($this->orderLogs->toCollection()->orderBy('createdAt', 'DESC')->limitBy(1) as $orderLog)
        {
            return $orderLog;
        }
        return NULL;
    }
}