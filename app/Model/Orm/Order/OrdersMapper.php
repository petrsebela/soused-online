<?php

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Mapper\Mapper;
use Tracy\Debugger;

class OrdersMapper extends Mapper
{
    public function findOrdersForCourier(User $user, string $orderStates)
    {
        $builder = $this->connection->query(
           "
                SELECT
                        orders.ean_code EAN,
                        orders.id ID,
                        orders.contact_name CONTACT_NAME,
                        orders.contact_surname CONTACT_SURNAME,
                        zips.zip ZIP,
                        orders.phone_number PHONE_NR,
                        orders.email EMAIL,
                        orders.name_invoicing INVOICING_NAME,
                        orders.surname_invoicing INVOICING_SURNAME,
                        orders.street STREET,
                        orders.address_number ADDRESS_NUMBER,
                        c1.name CITY,
                        c2.name INVOICING_CITY,
                        delivery_types.type DELIVERY_TYPE,
                        (SELECT order_logs.order_state_id FROM order_logs WHERE order_logs.order_id = orders.id ORDER BY order_logs.created_at DESC LIMIT 1) ORDER_STATE,
                        payment_types.type PAY_TYPE
                FROM orders
                    LEFT JOIN order_logs ON orders.id = order_logs.order_id
                    LEFT JOIN cities c1 ON orders.city_id = c1.id
                    LEFT JOIN cities c2 ON orders.invoicing_city_id = c2.id
                    LEFT JOIN zips ON orders.zip_id = zips.id
                    LEFT JOIN delivery_types ON orders.delivery_type_id = delivery_types.id
                    LEFT JOIN payment_types ON orders.payment_type_id = payment_types.id
                    WHERE (SELECT order_logs.order_state_id FROM order_logs WHERE order_logs.order_id = orders.id ORDER BY order_logs.created_at DESC LIMIT 1) IN (".$orderStates.");
           "
        );

        $result = $builder->fetchAll();
        return $result;
    }
}