<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class OrderItemState
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum self::TYPE_*}
 * @property string $description
 * @property HasMany|OrderItem[]|NULL $orderItems {1:m OrderItem::$orderItemState}
 */
class OrderItemState extends Entity
{
    const TYPE_OK = "OK";
    const TYPE_DAMAGED = "DAMAGED";
    const TYPE_NOT_TAKEN = "NOT_TAKEN";
}