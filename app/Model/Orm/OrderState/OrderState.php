<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class OrderState
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum self::TYPE_*}
 * @property string $description
 * @property HasMany|OrderLog[] $orderLogs {1:m OrderLog::$orderState}
 */
class OrderState extends Entity
{
    const TYPE_CREATED = "CREATED";
    const TYPE_ASSIGNED_TO_BRANCH = "ASSIGNED_TO_BRANCH";
    const TYPE_READY = "READY";
    const TYPE_ACCEPTED = "ACCEPTED";
    const TYPE_DELIVERING = "DELIVERING";
    const TYPE_DELIVERED = "DELIVERED";
    const TYPE_COMPLETED = "COMPLETED";
    const TYPE_ON_HOLD = "ON_HOLD";
    const TYPE_DAMAGED = "DAMAGED";
    const TYPE_NOT_TAKEN = "NOT_TAKEN";
    const TYPE_ASSIGNED_TO_COURRIER = "ASSIGNED_TO_COURRIER";
    const TYPE_STORNO = "STORNO";
}