<?php

namespace App\Model;

use Nextras\Orm\Entity\Entity;

/**
 * User
 * @property int $id {primary}
 * @property string $email
 * @property string|NULL $password
 * @property string $name
 * @property string $surname
 * @property Role|NULL $role {m:1 Role::$users}
 * @property bool $active {default false}
 * @property string|NULL $resetToken
 * @property string|NULL $phoneNumber
 * @property \DateTimeImmutable|NULL $lastLogin
 * @property \DateTimeImmutable|NULL $createdAt {default now}
 * @property Partner|NULL $partner {1:1 Partner::$adminUser}
 * @property PartnerBranch|NULL $branch {1:1 PartnerBranch::$branchUser}
 * @property VehicleLog[] $vehicleLogs {1:m VehicleLog::$createdBy}
 * @property string $defaultLang {default 'cs'}
 * @property bool|NULL $workingNow
 * @property OrderLog[] $orderLogs {1:m OrderLog::$createdBy}
 * @property OrderLog[] $orderLogAssigned {1:m OrderLog::$assignedTo}
 * @property CourierLog[] $courierLogs {1:m CourierLog::$courier}
 */
class User extends Entity
{

}
