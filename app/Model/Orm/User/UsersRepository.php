<?php

namespace App\Model;

use Nextras\Orm\Collection\ICollection;
use Nextras\Orm\Repository\Repository;

class UsersRepository extends Repository
{
	static function getEntityClassNames(): array{
		return [User::class];
	}

    public function getLastCourierOrderLog(User $courier): ?OrderLog
    {
        foreach ($courier->orderLogs->toCollection()->orderBy('createdAt', 'DESC')->limitBy(1) as $orderLog)
        {
            return $orderLog;
        }
        return NULL;
    }

    public function findCourierUndeliveredOrders(User $courier): ICollection
    {
        //todo distinct orderId from orderlogs order by created descending
    }
}