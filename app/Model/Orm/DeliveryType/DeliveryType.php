<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Model\Enum\DeliveryTypeEnum;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;

/**
 * Class DeliveryType
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum DeliveryTypeEnum::DELIVERY_TYPE_*}
 * @property string $description
 * @property HasMany|Order[] $orders {1:m Order::$deliveryType}
 */
class DeliveryType extends Entity
{

}