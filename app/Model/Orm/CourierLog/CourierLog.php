<?php
declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Nextras\Orm\Relationships\HasMany;
use Nextras\Orm\Relationships\HasOne;
use Model\Enum\CourierLogEnum;

/**
 * Class CourierLog
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum CourierLogEnum::TYPE_*}
 * @property User|NULL $courier {m:1 User::$courierLogs}
 * @property \DateTimeImmutable $createdAt {default now}
 */
class CourierLog extends Entity
{


}