<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 21. 9. 2020
 * Time: 22:45
 */

namespace App\Model;

use Nextras\Orm\Model\Model;

/**
 * Model
 * @property-read VehiclesRepository $vehicles
 * @property-read PartnersRepository $partners
 * @property-read UsersRepository $users
 * @property-read RolesRepository $roles
 * @property-read CitiesRepository $cities
 * @property-read ZipsRepository $zips
 * @property-read OrdersRepository $orders
 * @property-read PartnerBranchesRepository $partnersBranches
 * @property-read DeliveryTypesRepository $deliveryTypes
 * @property-read DocumentsRepository $documents
 * @property-read PaymentTypesRepository $paymentTypes
 * @property-read VehicleLogsRepository $vehicleLogs
 * @property-read ActionsRepository $actions
 * @property-read RightsRepository $rights
 * @property-read SettingsRepository $settings
 * @property-read OrderItemsRepository $orderItems
 * @property-read OrderItemStatesRepository $orderItemStates
 * @property-read OrderLogsRepository $orderLogs
 * @property-read OrderStatesRepository $orderStates
 * @property-read KnowledgeRepository $knowledge
 * @property-read CourierLogsRepository $courierLogs
 */
class Orm extends Model
{

}