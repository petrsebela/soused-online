<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 24. 9. 2020
 * Time: 20:35
 */

declare(strict_types=1);

namespace App\Model;

use Nextras\Orm\Entity\Entity;
use Model\Enum\PaymentTypeEnum;
use Nextras\Orm\Relationships\HasMany;

/**
 * Class PaymentType
 * @package App\Model
 * @property int $id {primary}
 * @property string $type {enum PaymentTypeEnum::PAYMENT_TYPE_*}
 * @property string $description
 * @property bool $active {default false}
 * @property HasMany|Order[] $orders {1:m Order::$paymentType}
 */
class PaymentType extends Entity
{

}