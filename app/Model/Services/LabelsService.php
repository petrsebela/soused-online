<?php

namespace App\Services;

use App\Model\Order;
use Contributte\Translation\Translator;
use Mpdf\Mpdf;

class LabelsService
{
    public Translator $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function createLabel(Order $order)
    {
        $pdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => [150,70],
            'margin-top' => 0,
            'margin-bottom' => 0,
            'margin-footer' => 0,
            'margin-header' => 0,
        ]);

        $html = '<table class="bpmTopnTail" style="width: 100%">
                    <tbody>
                        <tr>
                            <td colspan="2" style="text-align: center"><img src="/images/logo-square.PNG" width="100"></td>
                        </tr>
                        <tr>
                            <th>Jméno a příjmení:</th>
                            <td>'.$order->contactName . ' ' . $order->contactSurname .'</td>
                        </tr>
                        <tr>
                            <th>Datum doručení:</th>
                            <td>'.$order->deliveryDate->format("d.m.Y H:i") .'</td>
                        </tr>
                        <tr>
                            <th>Adresa doručení:</th>
                            <td>'. $order->street . ' ' . $order->addressNumber . ', ' . $order->city->name . ' ' . $order->zip->zip .'</td>
                        </tr>
                         <tr>
                            <th>Telefon:</th>
                            <td>'. $order->phoneNumber .'</td>
                        </tr>
                         <tr>
                            <th>Platba:</th>
                            <td>'. $this->translator->translate('entity.paymentType.type'.$order->paymentType->type) .'</td>
                        </tr>
                    </tbody>
                </table>';

        $stylesheet = file_get_contents(WWW_DIR.'/assets/mpdfstyletables.css');

        $pdf->WriteHTML($stylesheet, 1);
        $pdf->WriteHTML($html);
        return $pdf->Output('soused-online-label-'.$order->id.'.pdf', 'I');
    }
}