<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:25
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\AppModule\Components\Datagrids\DocumentsDatagrid;
use App\AppModule\Components\DocumentsDatagridFactory;
use App\AppModule\Forms\DocumentForm;
use App\AppModule\Forms\IDocumentFormFactory;
use App\AppModule\Forms\IKnowledgeFormFactory;
use App\AppModule\Forms\KnowledgeForm;
use App\Model\Document;
use App\Model\Knowledge;
use Nette\ComponentModel\IComponent;
use Tracy\Debugger;

class KnowledgePresenter extends BaseAppPresenter
{

    /** @inject */
    public IKnowledgeFormFactory $knowledgeFormFactory;

    public ?Knowledge $knowledge;

    public function actionEdit(int $id = null): void
    {
        if ($id) {
            try {
                $this->knowledge = $this->orm->knowledge->getById($id);
            } catch (\Exception $exception) {
                Debugger::log($exception);
            }
        } else {
            $this->knowledge = null;
        }
    }

    public function renderEdit(): void
    {
        $this->template->item = $this->knowledge;
    }


    public function createComponentKnowledgeForm(string $name): KnowledgeForm
    {
        return $this->knowledgeFormFactory->create($this->knowledge);
    }

    public function actionDefault()
    {
        $knowledgeContent = $this->orm->knowledge->findAll()->orderBy('id', 'ASC');
        $this->template->knowledgeContent = $knowledgeContent;
    }

    public function actionDetail($id)
    {
        $detail = $this->orm->knowledge->getById($id);
        $this->knowledge=$detail;
        $this->template->detail = $detail;
    }
}