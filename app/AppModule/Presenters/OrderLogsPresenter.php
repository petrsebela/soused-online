<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:23
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;


use App\AppModule\Components\Datagrids\OrderLogsDatagrid;
use App\AppModule\Components\Datagrids\VehicleLogsDatagrid;
use App\AppModule\Components\OrderLogsDatagridFactory;
use App\AppModule\Components\VehicleLogsDatagridFactory;
use App\AppModule\Forms\IOrderLogFormFactory;
use App\AppModule\Forms\IVehicleLogFormFactory;
use App\AppModule\Forms\OrderForm;
use App\AppModule\Forms\VehicleLogForm;
use App\Model\OrderLog;
use App\Model\VehicleLog;
use Tracy\Debugger;

class OrderLogsPresenter extends BaseAppPresenter
{
    /** @inject */
    public OrderLogsDatagridFactory $orderLogsDatagridFactory;

    /** @inject */
    public IOrderLogFormFactory $orderLogFormFactory;

    public ?OrderLog $orderLog;

    public function actionEdit(int $id = null): void
    {
        if ($id) {
            try {
                $this->orderLog = $this->orm->orderLogs->getById($id);
            } catch (\Exception $exception) {
                Debugger::log($exception);
            }
        } else {
            $this->orderLog = null;
        }
    }

    public function renderEdit(): void
    {
        $this->template->item = $this->orderLog;
    }


    public function createComponentOrderLogsDatagrid(): OrderLogsDatagrid
    {
        return $this->orderLogsDatagridFactory->create();
    }

    public function createComponentOrderLogsForm(): OrderForm
    {
        return $this->orderLogFormFactory->create($this->orderLog);
    }
}