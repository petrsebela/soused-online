<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 21. 9. 2020
 * Time: 23:32
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\Model\Order;
use App\Model\OrderLog;
use App\Model\OrderState;
use App\Model\Role;
use Nextras\Orm\Collection\ICollection;

class DashboardPresenter extends BaseAppPresenter
{
    public function renderDefault()
    {
        $now = new \DateTimeImmutable();
        $this->template->turnover = $this->orm->orders->getDayTurnover($now);
        $this->template->ordersQuantity = $this->orm->orders->getDayOrdersSold($now);
        $this->template->productsQuantity = $this->orm->orders->getDayProductsSold($now);

        $this->template->activeUsers = $this->orm->users->findBy(['workingNow' => true]);
        $this->template->couriersOnWay = [];//$this->getCouriersOnWay();
        $this->template->actualOrders = $this->orm->orders->findAll(); //todo
    }

    private function getCouriersOnWay(): ICollection
    {
        foreach ($this->orm->users->findBy(['role' => $this->orm->roles->getBy(['intName' => Role::INT_NAME_COURIER]), 'workingNow' => true]) as $courier)
        {
            $lastLog = $this->orm->users->getLastCourierOrderLog($courier);
            if ($this->orm->orders->getLastOrderLog($lastLog->order)->orderState === 'DELIVERING') //todo do enumu
            {

            }
        }
    }
}