<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:24
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\AppModule\Components\MenuComponent;
use App\AppModule\Forms\ISettingsFormFactory;
use App\AppModule\Forms\SettingsForm;

class SystemSettingsPresenter extends BaseAppPresenter
{
    /** @inject */
    public ISettingsFormFactory $settingsFormFactory;

    public function createComponentSettingsForm(string $name): SettingsForm
    {
        return $this->settingsFormFactory->create();
    }
}