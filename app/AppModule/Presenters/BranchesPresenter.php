<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:21
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\AppModule\Components\BranchOrdersDatagridFactory;
use App\AppModule\Components\Datagrids\BranchOrdersDatagrid;
use App\AppModule\Components\Datagrids\PartnerBranchesDatagrid;
use App\AppModule\Components\PartnerBranchesDatagridFactory;
use App\AppModule\Forms\IPartnerBranchFormFactory;
use App\AppModule\Forms\PartnerBranchForm;
use App\Model\PartnerBranch;
use Tracy\Debugger;

class BranchesPresenter extends BaseAppPresenter
{
    /** @inject */
    public PartnerBranchesDatagridFactory $partnerBranchesDatagridFactory;

    /** @inject */
    public IPartnerBranchFormFactory $partnerBranchFormFactory;

    public ?PartnerBranch $partnerBranch;

    /** @inject */
    public BranchOrdersDatagridFactory $branchOrdersDatagridFactory;

    public function actionEdit(int $id = null): void
    {
        if ($id) {
            try {
                $this->partnerBranch = $this->orm->partnersBranches->getById($id);
            } catch (\Exception $exception) {
                Debugger::log($exception);
            }
        } else {
            $this->partnerBranch = null;
        }
    }

    public function renderEdit(): void
    {
        $this->template->item = $this->partnerBranch;
    }

    public function actionDetail(int $id = null): void
    {
        if ($id) {
            try {
                $this->partnerBranch = $this->orm->partnersBranches->getById($id);
            } catch (\Exception $exception) {
                Debugger::log($exception);
            }
        } else {
            $this->partnerBranch = null;
        }
    }

    public function renderDetail(): void
    {
        $this->template->item = $this->partnerBranch;
    }

    public function createComponentPartnerBranchesDatagrid(string $name): PartnerBranchesDatagrid
    {
        return $this->partnerBranchesDatagridFactory->create($this,null);
    }

    public function createComponentBranchOrdersDatagrid(string $name): BranchOrdersDatagrid
    {
        return $this->branchOrdersDatagridFactory->create($this->partnerBranch);
    }

    public function createComponentPartnerBranchForm(): PartnerBranchForm
    {
        return $this->partnerBranchFormFactory->create($this->partnerBranch);
    }

    public function handleDelete(int $id): void
    {
        $item = $this->orm->partnersBranches->getById($id);

        if ($item)
        {
            $item->deleted = true;
            $this->orm->persistAndFlush($item);
        }

        if ($this->isAjax()) {
            $this['partnerBranchesDatagrid']->reload();
        } else {
            $this->redirect('this');
        }
    }
}