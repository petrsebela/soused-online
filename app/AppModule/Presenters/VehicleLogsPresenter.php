<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:23
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;


use App\AppModule\Components\Datagrids\VehicleLogsDatagrid;
use App\AppModule\Components\VehicleLogsDatagridFactory;
use App\AppModule\Forms\IVehicleLogFormFactory;
use App\AppModule\Forms\VehicleLogForm;
use App\Model\VehicleLog;
use Tracy\Debugger;

class VehicleLogsPresenter extends BaseAppPresenter
{
    /** @inject */
    public VehicleLogsDatagridFactory $vehicleLogsDatagridFactory;

    /** @inject */
    public IVehicleLogFormFactory $vehicleLogFormFactory;

    public ?VehicleLog $vehicleLog;

    public function actionEdit(int $id = null): void
    {
        if ($id) {
            try {
                $this->vehicleLog = $this->orm->vehicleLogs->getById($id);
            } catch (\Exception $exception) {
                Debugger::log($exception);
            }
        } else {
            $this->vehicleLog = null;
        }
    }

    public function renderEdit(): void
    {
        $this->template->item = $this->vehicleLog;
    }

    public function createComponentVehicleLogsDatagrid(string $name): VehicleLogsDatagrid
    {
        return $this->vehicleLogsDatagridFactory->create();
    }

    public function createComponentVehicleLogsForm(): VehicleLogForm
    {
        return $this->vehicleLogFormFactory->create($this->vehicleLog);
    }
}