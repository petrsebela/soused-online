<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:23
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\AppModule\Components\CourierAccidentsDatagridFactory;
use App\AppModule\Components\CourierOrdersDatagridFactory;
use App\AppModule\Components\CourierVehiclesLogsDatagridFactory;
use App\AppModule\Components\Datagrids\CourierAccidentsDatagrid;
use App\AppModule\Components\Datagrids\CourierOrdersDatagrid;
use App\AppModule\Components\Datagrids\CourierVehiclesLogsDatagrid;
use App\AppModule\Components\Datagrids\OperatorLogsDatagrid;
use App\AppModule\Components\Datagrids\CouriersDatagrid;
use App\AppModule\Components\OperatorLogsDatagridFactory;
use App\AppModule\Components\CouriersDatagridFactory;
use App\Model\User;

class CouriersPresenter extends BaseAppPresenter
{
    public bool $active;

    public User $courierUser;

    /** @inject  */
    public CouriersDatagridFactory $couriersDatagridFactory;

    /** @inject */
    public CourierOrdersDatagridFactory $courierOrdersDatagridFactory;

    /** @inject  */
    public CourierVehiclesLogsDatagridFactory $courierVehiclesLogsDatagridFactory;

    /** @inject */
    public CourierAccidentsDatagridFactory  $courierAccidentsDatagridFactory;

    public function actionDefault(bool $active = true): void
    {
        $this->active = $active;
    }

    public function renderDefault(): void
    {
        $this->template->active = $this->active;
    }

    public function createComponentCouriersDatagrid(string $name): CouriersDatagrid
    {
        return $this->couriersDatagridFactory->create($this->active);
    }

    public function createComponentCourierOrdersDatagrid(string $name): CourierOrdersDatagrid
    {
        return $this->courierOrdersDatagridFactory->create($this->courierUser);
    }

    public function createComponentCourierVehiclesLogsDatagrid(string $name): CourierVehiclesLogsDatagrid
    {
        return $this->courierVehiclesLogsDatagridFactory->create($this->courierUser);
    }

    public function createComponentCourierIncidentsLogsDatagrid(string $name): CourierAccidentsDatagrid
    {
        return $this->courierAccidentsDatagridFactory->create($this->courierUser);
    }

    public function actionDetail(int $id): void
    {
        $this->courierUser = $this->orm->users->getById($id);
    }

    public function renderDetail(): void
    {
        $this->template->item = $this->courierUser;
    }
}