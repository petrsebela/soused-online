<?php
/**
 * Created by PhpStorm.
 * User: Petr Šebela
 * Date: 22. 9. 2020
 * Time: 17:23
 */

declare(strict_types=1);

namespace App\AppModule\Presenters;

use App\AppModule\Components\Datagrids\OperatorLogsDatagrid;
use App\AppModule\Components\Datagrids\OperatorsDatagrid;
use App\AppModule\Components\OperatorLogsDatagridFactory;
use App\AppModule\Components\OperatorsDatagridFactory;
use App\Model\User;
use Nette\ComponentModel\IComponent;

class OperatorsPresenter extends BaseAppPresenter
{
    public bool $active;

    public User $operatorUser;

    /** @inject  */
    public OperatorsDatagridFactory $operatorsDatagridFactory;

    /** @inject  */
    public OperatorLogsDatagridFactory $operatorLogsDatagridFactory;

    public function actionDefault(bool $active = true): void
    {
        $this->active = $active;
    }

    public function renderDefault(): void
    {
        $this->template->active = $this->active;
    }

    public function createComponentOperatorsDatagrid(string $name): OperatorsDatagrid
    {
        return $this->operatorsDatagridFactory->create($this->active);
    }

    public function createComponentOperatorLogsDatagrid(string $name): OperatorLogsDatagrid
    {
        return $this->operatorLogsDatagridFactory->create($this->operatorUser);
    }

    public function actionDetail(int $id): void
    {
        $this->operatorUser = $this->orm->users->getById($id);
    }

    public function renderDetail(): void
    {
        $this->template->item = $this->operatorUser;
    }
}