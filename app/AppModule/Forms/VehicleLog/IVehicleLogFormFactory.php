<?php
declare(strict_types=1);

namespace App\AppModule\Forms;

use App\Model\VehicleLog;

interface IVehicleLogFormFactory
{
    function create(?VehicleLog $vehicleLog): VehicleLogForm;
}