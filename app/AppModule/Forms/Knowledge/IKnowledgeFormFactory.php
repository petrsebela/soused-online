<?php
declare(strict_types=1);

namespace App\AppModule\Forms;

use App\Model\Knowledge;

interface IKnowledgeFormFactory
{
    function create(?Knowledge $knowledge): KnowledgeForm;
}