<?php
declare(strict_types=1);

namespace App\AppModule\Components;

use App\AppModule\Components\Datagrids\CourierAccidentsDatagrid;
use App\AppModule\Components\Datagrids\CourierOrdersDatagrid;
use App\AppModule\Components\Datagrids\CourierVehiclesLogsDatagrid;
use App\Model\Orm;
use App\Model\User;
use Contributte\Translation\Translator;

class CourierOrdersDatagridFactory
{
    private Translator $translator;

    private Orm $orm;

    public function __construct(Orm $orm, Translator $translator)
    {
        $this->translator = $translator;
        $this->orm = $orm;
    }

    public function create(User $courierUser): CourierOrdersDatagrid
    {
        $datagrid = new CourierOrdersDatagrid($courierUser, $this->orm);
        $datagrid->setTranslator($this->translator);
        $datagrid->setup();

        return $datagrid;
    }
}