<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\OrderLog;
use App\Model\Orm;
use App\Model\Order;
use App\Model\User;
use Nette;
use Nette\Utils\Html;

class CourierOrdersDatagrid extends BasicDatagrid
{
    protected Orm $orm;
    public User $courierUser;

    public function __construct(User $courierUser, Orm $orm, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
        $this->courierUser = $courierUser;
    }

    public function setup(): void
    {
        $domain = "entity.order";

        $orderLogs = $this->orm->orderLogs->findBy(['assignedTo' => $this->courierUser])->orderBy('createdAt', 'DESC');
        $this->setDataSource($orderLogs);

        $this->setColumnsHideable();

        $this->addColumnText("createdAt", $domain.".createdAt")
            ->setRenderer(function (OrderLog $item) {
                return $item->createdAt->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("eanCode", $domain.".eanCode")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("deliveryDate", $domain.".deliveryDate")
            ->setRenderer(function (OrderLog $item) {
                return $item->order->deliveryDate->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("phoneNumber", $domain.".phoneNumber")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("contactName", $domain.".contactName")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("contactSurname", $domain.".contactSurname")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("partnerBranch", $domain.".partnerBranch")
            ->setSortable()
            ->setRenderer(function (OrderLog $item) {
                return Html::el('a', [
                    'href' => $this->getPresenter()->link('Branches:detail', ['id' => $item->order->partnerBranch->id]),
                    'target' => '_blank'
                ])->setText($item->order->partnerBranch ? $item->order->partnerBranch->name : 'common.notAssigned');
            })
            ->setFilterSelect([null => '']+$this->orm->partnersBranches->findAll()->fetchPairs('id', 'name'));

        $this->addColumnText("paymentType", $domain.".paymentType")
            ->setRenderer(function (OrderLog $item) {
                return $this->translator->translate('entity.paymentType.type'.$item->order->paymentType->type);
            })
            ->setSortable();

        $this->addColumnText("deliveryType", $domain.".deliveryType")
            ->setRenderer(function (OrderLog $item) {
                return $this->translator->translate('entity.deliveryType.type'.$item->order->deliveryType->type);
            })
            ->setSortable();

        $this->addColumnText("addressNumber", $domain.".addressNumber")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("street", $domain.".street")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("city", $domain.".city")
            ->setRenderer(function (OrderLog $item) {
                return $item->order->city->name;
            })
            ->setSortable()
            ->setFilterMultiSelect($this->orm->cities->findAll()->fetchPairs('id', 'name'));

        $this->addColumnText("zip", $domain.".zip")
            ->setRenderer(function (OrderLog $item) {
                return $item->order->zip->zip;
            })
            ->setSortable()
            ->setDefaultHide();


        $this->addAction('detail', 'Detail', 'Orders:detail')
            ->setClass('btn btn-success btn-sm')
            ->setRenderer(function (OrderLog $orderLog) {
                return Html::el('a', ['href'=>$this->presenter->link('Orders:detail', ['id' => $orderLog->order->id]), 'target' => '_blank', 'class' => 'btn btn-success btn-sm'])
                    ->setText('detail');
            });
    }
}