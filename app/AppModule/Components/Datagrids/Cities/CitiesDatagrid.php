<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\City;
use App\Model\Orm;
use Nette;

class CitiesDatagrid extends BasicDatagrid
{
    protected Orm $orm;

    public function __construct(Orm $orm, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
    }

    public function setup(): void
    {
        $domain = "entity.city";

        $this->setDataSource($this->orm->cities->findAll());

        $this->addColumnText('id', $this->translator->translate('common.id'))
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("name", $this->translator->translate($domain.".name"))
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("zips", $this->translator->translate($domain.".zips"))
            ->setRenderer(function (City $city) {
                $zips = '';
                foreach ($city->zips as $zip)
                {
                    $zips .= $zip->zip.', ';
                }
                return rtrim($zips, ', ');
            });

        $this->addAction('edit', $this->translator->translate('common.edit'))
            ->setClass('btn btn-success btn-sm');
    }
}