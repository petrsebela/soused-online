<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\Utils\StringUtils;
use ReflectionClass;

abstract class BasicDatagrid extends BaseDatagrid implements ISetupDatagrid
{

}