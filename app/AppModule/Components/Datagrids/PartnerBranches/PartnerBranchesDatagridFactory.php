<?php
declare(strict_types=1);

namespace App\AppModule\Components;

use App\AppModule\Components\Datagrids\PartnerBranchesDatagrid;
use App\AppModule\Presenters\BaseAppPresenter;
use App\Model\Orm;
use App\Model\Partner;
use Contributte\Translation\Translator;

class PartnerBranchesDatagridFactory
{
    private Translator $translator;

    private Orm $orm;

    public function __construct(Orm $orm, Translator $translator)
    {
        $this->translator = $translator;
        $this->orm = $orm;
    }

    public function create(BaseAppPresenter $presenter, ?Partner $partner): PartnerBranchesDatagrid
    {
        $datagrid = new PartnerBranchesDatagrid($this->orm, $partner);
        $datagrid->setTranslator($this->translator);
        $datagrid->setup($presenter);

        return $datagrid;
    }
}