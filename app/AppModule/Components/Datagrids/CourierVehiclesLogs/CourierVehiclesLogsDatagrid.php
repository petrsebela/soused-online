<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\OrderLog;
use App\Model\Orm;
use App\Model\Order;
use App\Model\User;
use App\Model\VehicleLog;
use Model\Enum\VehicleLogEnum;
use Nette;
use Nette\Utils\Html;

class CourierVehiclesLogsDatagrid extends BasicDatagrid
{
    protected Orm $orm;
    public User $courierUser;

    public function __construct(User $courierUser, Orm $orm, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
        $this->courierUser = $courierUser;
    }

    public function setup(): void
    {
        $domain = "entity.vehicleLog";

        $orderLogs = $this->orm->vehicleLogs->findBy(['createdBy' => $this->courierUser, 'event!=' => VehicleLogEnum::VEHICLE_LOG_ACCIDENT])->orderBy('createdAt', 'DESC');
        $this->setDataSource($orderLogs);

        $this->setColumnsHideable();

        $this->addColumnText("createdAt", $domain.".createdAt")
            ->setRenderer(function (VehicleLog $item) {
                return $item->createdAt->format('d.m.Y H:i');
            }) ;

        $this->addColumnText("event", $domain.".event")
            ->setRenderer(function (VehicleLog $item) {
                return $this->translator->translate('entity.vehicleLog.event'.$item->event);
            });

        $this->addColumnText("vehicle", $domain.".vehicleId")
            ->setRenderer(function (VehicleLog $item) {
                return $item->vehicle->spz;
            });
    }
}