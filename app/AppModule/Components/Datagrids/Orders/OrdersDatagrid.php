<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\DeliveryType;
use App\Model\Enum\FlashMessages;
use App\Model\OrderState;
use App\Model\Orm;
use App\Model\Order;
use App\Model\PaymentType;
use App\Model\Role;
use App\Model\User;
use Nette;
use Nette\Utils\Html;

class OrdersDatagrid extends BasicDatagrid
{
    protected Orm $orm;
    protected Nette\Application\UI\Presenter $presenter;

    public function __construct(Orm $orm, Nette\Application\UI\Presenter $presenter, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
        $this->presenter = $presenter;
    }

    public function setup(): void
    {
        $domain = "entity.order";

        /** @var User $user */
        $user = $this->presenter->getUser()->user;

        if ($this->presenter->getUser()->isInRole(Role::INT_NAME_COURIER)) {
            $states = $this->orm->orderStates->findBy(['type' => [OrderState::TYPE_ASSIGNED_TO_COURRIER, OrderState::TYPE_STORNO, OrderState::TYPE_DELIVERING, OrderState::TYPE_DAMAGED, OrderState::TYPE_NOT_TAKEN, OrderState::TYPE_ON_HOLD]]);
            $orderStates = '';
            foreach ($states as $state)
            {
                $orderStates .= $state->id . ',';
            }
            $orderStates = rtrim($orderStates, ',');
            $this->setDataSource($this->orm->orders->findOrdersForCourier($this->presenter->getUser()->user, $orderStates));
        } elseif($this->presenter->getUser()->isInRole(Role::INT_NAME_BRANCH)) {
            $this->setDataSource($this->orm->orders->findBy(['partnerBranch->id' => $user->branch->id]));
        } elseif($this->presenter->getUser()->isInRole(Role::INT_NAME_PARTNER)) {
            $this->setDataSource($this->orm->orders->findBy(['partnerBranch->partner->id' => $user->partner->id]));
        } else {
            $this->setDataSource($this->orm->orders->findAll());
        }


        $this->setColumnsHideable();

        $this->addColumnText('id', 'common.id')
            ->setSortable()
            ->setDefaultHide()
            ->setFilterText();

        $this->addColumnText("createdAt", $domain.".createdAt")
            ->setRenderer(function (Order $item) {
                return $item->createdAt->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("eanCode", $domain.".eanCode")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("deliveryDate", $domain.".deliveryDate")
            ->setRenderer(function (Order $item) {
                return $item->deliveryDate->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("phoneNumber", $domain.".phoneNumber")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("contactName", $domain.".contactName")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("contactSurname", $domain.".contactSurname")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("partnerBranch", $domain.".partnerBranch")
            ->setRenderer(function (Order $item) {
                return Html::el('a', [
                    'href' => $this->getPresenter()->link('Branches:detail', ['id' => $item->partnerBranch->id]),
                    'target' => '_blank'
                ])->setText($item->partnerBranch ? $item->partnerBranch->name : 'common.notAssigned');
            })
            ->setFilterSelect([null => 'všechny']+$this->orm->partnersBranches->findAll()->fetchPairs('id', 'name'));

        $paymentsArray = [];
        $payments = $this->orm->paymentTypes->findAll();
        /** @var PaymentType $payment */
        foreach ($payments as $payment)
        {
            $paymentsArray[$payment->id] = $this->translator->translate('entity.paymentType.type'.$payment->type);
        }
        $this->addColumnText("paymentType", $domain.".paymentType")
            ->setRenderer(function (Order $item) {
                return $this->translator->translate('entity.paymentType.type'.$item->paymentType->type);
            })
            ->setFilterSelect([null => 'všechny'] + $paymentsArray);

        $deliveriesArray = [];
        $deliveries = $this->orm->deliveryTypes->findAll();
        /** @var DeliveryType $delivery */
        foreach ($deliveries as $delivery)
        {
            $deliveriesArray[$delivery->id] = $this->translator->translate('entity.deliveryType.type'.$delivery->type);
        }
        $this->addColumnText("deliveryType", $domain.".deliveryType")
            ->setRenderer(function (Order $item) {
                return $this->translator->translate('entity.deliveryType.type'.$item->deliveryType->type);
            })
            ->setFilterSelect([null => 'všechny'] + $deliveriesArray);

        $this->addColumnText("addressNumber", $domain.".addressNumber")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("street", $domain.".street")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("city", $domain.".city")
            ->setRenderer(function (Order $order) {
                return $order->city->name;
            })
            ->setSortable()
            ->setFilterMultiSelect($this->orm->cities->findAll()->fetchPairs('id', 'name'));

        $this->addColumnText("zip", $domain.".zip")
            ->setRenderer(function (Order $order) {
                return $order->zip->zip;
            })
            ->setSortable()
            ->setDefaultHide()
            ->setFilterMultiSelect($this->orm->zips->findAll()->fetchPairs('id', 'zip'));

        $this->addColumnText("email", $domain.".email")
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("ic", $domain.".ic")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("dic", $domain.".dic")
            ->setSortable()
            ->setDefaultHide()
            ->setFilterText();

        $this->addColumnText("nameInvoicing", $domain.".nameInvoicing")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("surnameInvoicing", $domain.".surnameInvoicing")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("streetInvoicing", $domain.".streetInvoicing")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("invoicingCity", $domain.".invoicingCity")
            ->setRenderer(function (Order $order) {
                return $order->invoicingCity->name;
            })
            ->setSortable()
            ->setFilterMultiSelect($this->orm->cities->findAll()->fetchPairs('id', 'name'));

        $this->addColumnText("invoicingZip", $domain.".invoicingZip")
            ->setRenderer(function (Order $order) {
                return $order->invoicingZip->zip;
            })
            ->setSortable()
            ->setDefaultHide()
            ->setFilterMultiSelect($this->orm->zips->findAll()->fetchPairs('id', 'zip'));

        $this->addColumnText("remark", $domain.".remark")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addColumnText("orderSource", $domain.".orderSource")
            ->setDefaultHide()
            ->setSortable()
            ->setFilterText();

        $this->addAction('detail', 'Detail')
            ->setClass('btn btn-success btn-sm');
    }
}