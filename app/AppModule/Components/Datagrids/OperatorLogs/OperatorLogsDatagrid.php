<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\Order;
use App\Model\Orm;
use App\Model\User;
use App\Model\VehicleLog;
use Contributte\Translation\Translator;
use Mpdf\Tag\Tr;
use Nette;

class OperatorLogsDatagrid extends BaseDatagrid
{
    protected Orm $orm;
    public User $user;

    public function __construct(Orm $orm, User $user, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
        $this->user = $user;
    }

    public function setup(): void
    {
        $domain = "entity.operatorLogs";

        $this->setDataSource($this->orm->vehicleLogs->findAll());

        $this->addColumnText('id', 'common.id')
            ->setSortable();

        $this->addColumnText("event", $domain.".event")
            ->setRenderer(function (VehicleLog $item){
              return $this->translator->translate('entity.vehicleLog.event'.$item->event);
            })
            ->setSortable()
            ->setFilterText();


        $this->addColumnText("createdAt", $domain.".createdAt")
            ->setRenderer(function (VehicleLog $item) {
                return $item->createdAt->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("createdBy", $domain.".createdBy")
            ->setRenderer(function (VehicleLog $item) {
                return $item->createdBy?$item->createdBy->surname:'';
            })
            ->setSortable()
            ->setFilterMultiSelect($this->orm->users->findAll()->fetchPairs('id', 'surname'));


        $this->addColumnText("vehicle", $domain.".vehicle")
            ->setRenderer(function (VehicleLog $item) {
                return $item->vehicle->spz;
            })
            ->setSortable()
            ->setFilterMultiSelect($this->orm->vehicles->findAll()->fetchPairs('id', 'spz'));



        /*
        $this->addAction('edit', 'common.edit')
            ->setClass('btn btn-success btn-sm'); */
    }
}