<?php
declare(strict_types=1);

namespace App\AppModule\Components;

use App\AppModule\Components\Datagrids\OperatorLogsDatagrid;
use App\Model\Orm;
use App\Model\User;
use Contributte\Translation\Translator;

class OperatorLogsDatagridFactory
{
    private Translator $translator;

    private Orm $orm;

    public function __construct(Orm $orm, Translator $translator)
    {
        $this->translator = $translator;
        $this->orm = $orm;
    }

    public function create(User $user): OperatorLogsDatagrid
    {
        $datagrid = new OperatorLogsDatagrid($this->orm, $user);
        $datagrid->setTranslator($this->translator);
        $datagrid->setup();

        return $datagrid;
    }
}