<?php
declare(strict_types=1);

namespace App\AppModule\Components\Datagrids;

use App\Model\OrderLog;
use App\Model\Orm;
use App\Model\VehicleLog;
use Nette;
use Nette\Utils\Html;

class OrderLogsDatagrid extends BasicDatagrid
{
    protected Orm $orm;

    public function __construct(Orm $orm, Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($orm, $parent, $name);
        $this->orm = $orm;
    }

    public function setup(): void
    {
        $domain = "entity.orderLog";

        $this->setDataSource($this->orm->orderLogs->findAll()->orderBy('createdAt', 'DESC'));

        $this->setColumnsHideable();

        $this->addColumnText('id', 'common.id')
            ->setSortable()
            ->setDefaultHide()
            ->setFilterText();

        $this->addColumnText("createdAt", $domain.".createdAt")
            ->setRenderer(function (OrderLog $item) {
                return $item->createdAt->format('d.m.Y H:i');
            })
            ->setSortable()
            ->setFilterDate();

        $this->addColumnText("order", $domain.".order")
            ->setRenderer(function (OrderLog $item) {

                    return Html::el('a', [
                        'href' => $this->getPresenter()->link('Orders:detail', ['id' => $item->order->id]),
                        'target' => '_blank'
                    ])->setText($item->order->id);
            })
            ->setSortable()
            ->setFilterText();


        $orderStates = [];
        foreach ($this->orm->orderStates->findAll() as $orderState)
        {
            $orderStates[$orderState->id] = $this->translator->translate('entity.orderLog.orderState'.$orderState->type);
        }

        $this->addColumnText("orderState", $domain.".orderState")
            ->setRenderer(function (OrderLog $item) {
                return $this->translator->translate('entity.orderLog.orderState'.$item->orderState->type);
            })
            ->setSortable()
            ->setFilterSelect($orderStates)
            ->setPrompt('');

        $this->addColumnText("createdBy", $domain.".createdBy")
            ->setRenderer(function (OrderLog $item) {
                if ($item->createdBy) {
                    return Html::el('a', [
                        'href' => $this->getPresenter()->link('Users:detail', ['id' => $item->createdBy->id]),
                        'target' => '_blank'
                    ])->setText($item->createdBy ? $item->createdBy->name . ' ' . $item->createdBy->surname : 'common.notAssigned');
                } else {
                    return 'Nepřiřazeno';
                }
            })
            ->setSortable()
            ->setFilterSelect($this->orm->users->findAll()->fetchPairs('id', 'surname'))
            ->setPrompt('');

        $this->addColumnText("assignedTo", $domain.".assignedTo")
            ->setRenderer(function (OrderLog $item) {
                if ($item->assignedTo) {
                    return Html::el('a', [
                        'href' => $this->getPresenter()->link('Users:detail', ['id' => $item->assignedTo->id]),
                        'target' => '_blank'
                    ])->setText($item->assignedTo ? $item->assignedTo->name . ' ' . $item->assignedTo->surname : 'common.notAssigned');
                } else {
                    return 'Nepřiřazeno';
                }
            })
            ->setSortable()
            ->setFilterSelect($this->orm->users->findAll()->fetchPairs('id', 'surname'))
            ->setPrompt('');
    }
}