<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

final class RouterFactory
{
	use Nette\StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;

        $router[] = $routerApp = new RouteList("App");
        $routerApp[] = new Route('<presenter>/<action>[/<id>]', 'Authentication:default');

        $router[] = $routerWeb = new RouteList("Web");
        $routerWeb[] = new Route('web/<presenter>/<action>', 'Default:default');

		return $router;
	}
}
