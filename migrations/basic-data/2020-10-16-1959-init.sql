SET NAMES utf8mb4;

INSERT INTO `roles` (`id`, `int_name`) VALUES
(1,	'admin'),
(2,	'operator'),
(3,	'courier'),
(4,	'partner'),
(5,	'branch'),
(6,	'developer');