SET NAMES utf8mb4;

INSERT INTO `rights` (`id`, `name`) VALUES
(1,	'orders'),
(2,	'rights'),
(3,	'branches'),
(4,	'partners'),
(5,	'cities'),
(6,	'cms'),
(7,	'couriers'),
(8,	'deliveryTypes'),
(9,	'dashboard'),
(10,	'documents'),
(11,	'operators'),
(12,	'paymentTypes'),
(14,	'systemSettings'),
(15,	'vehicles'),
(16,	'zips'),
(17,	'users'),
(18,	'vehiclelogs'),
(19,	'orderlogs'),
(20,	'knowledge');


SET NAMES utf8mb4;

INSERT INTO `actions` (`id`, `name`, `right_id`) VALUES
(1,	'read',	1),
(2,	'read',	2),
(3,	'read',	3),
(4,	'read',	4),
(5,	'read',	5),
(6,	'read',	6),
(7,	'read',	7),
(8,	'read',	8),
(9,	'read',	9),
(10,	'read',	10),
(11,	'read',	11),
(12,	'read',	12),
(13,	'read',	13),
(14,	'read',	14),
(15,	'read',	15),
(16,	'read',	16),
(17,	'read',	17),
(18,	'read',	18),
(19,	'showstatsturnover',	9),
(20,	'showstatsorders',	9),
(21,	'showstatsorderitems',	9),
(22,	'showactualorders',	9),
(23,	'showactiveusers',	9),
(24,	'showcouriersonway',	9),
(25,	'read',	19),
(26,	'read',	20),
(27,	'edit',	3),
(28,	'showbasicinfo',	1);

