-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `api_keys` (`id`, `api_key`, `description`, `active`) VALUES
(1,	'SO-DEV-01',	NULL,	1);

SET NAMES utf8mb4;

INSERT INTO `cities` (`id`, `name`) VALUES
(1,	'Střelice u Brna'),
(2,	'Brno');


INSERT INTO `delivery_types` (`id`, `type`, `description`) VALUES
(1,	'STANDARD',	''),
(2,	'EXPRESS',	''),
(3,	'SUPER_EXPRESS',	'');

INSERT INTO `documents` (`id`, `name`, `partner_id`, `file_path`, `created_at`) VALUES
(1,	'Plná moc Jaso',	1,	'Plná-moc2020-11-08163340.odt',	'2020-11-08 16:33:31');

INSERT INTO `knowledge` (`id`, `heading`, `description`, `created_at`) VALUES
(1,	'Jak tankovat CNG',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sodales libero vel consectetur mattis. Sed ac ex id felis ornare cursus at vel nibh. Quisque elementum vehicula metus, sit amet finibus enim aliquet vitae. Pellentesque viverra eleifend nisi non lacinia. Aliquam sed sem vulputate, gravida velit eu, laoreet ligula. Sed vitae malesuada tortor. Aenean cursus, tortor a dignissim aliquet, nibh nibh vestibulum nibh, vel dignissim odio felis quis felis. Mauris viverra sem interdum faucibus hendrerit. In blandit sagittis elit, ac blandit lacus ornare sagittis. Curabitur ullamcorper faucibus nibh id congue. Praesent vel sapien risus. Nam tempor tristique justo, ac tristique arcu viverra ac. Nunc volutpat lectus ac posuere commodo. Quisque hendrerit, nisi at dictum iaculis, arcu tortor vulputate urna, eget semper urna est sed ipsum. Cras tempor est metus, in imperdiet nisi vestibulum sit amet.\r\n\r\nSuspendisse potenti. Donec condimentum sem nisi, sit amet ornare orci dapibus ac. Duis ac augue lectus. In sodales condimentum dolor vel varius. Praesent lectus enim, vehicula in accumsan nec, varius vel turpis. Sed consectetur erat in ante pulvinar congue. Cras a purus massa.\r\n\r\nSuspendisse turpis nisi, consequat eu est non, hendrerit viverra tellus. Aenean sed cursus ligula. Ut nec convallis tellus, et imperdiet nunc. Cras bibendum euismod tempor. Nunc id fermentum ex. Phasellus consectetur, lectus ac vestibulum ultrices, tortor ante faucibus nisi, nec vehicula felis nibh a orci. Donec hendrerit, purus in pharetra ultricies, massa nulla iaculis sapien, sit amet sagittis sem magna et diam. Phasellus ut ligula pellentesque, feugiat urna sit amet, ultricies diam. Nullam quis orci nec diam pharetra commodo. Sed vel libero lobortis, sollicitudin enim vitae, molestie eros. Cras in neque eu enim aliquam eleifend. Fusce massa sem, pretium a tortor quis, volutpat eleifend orci. Vivamus porta sapien nec vehicula congue. Mauris aliquet ligula arcu, eu posuere augue suscipit nec.',	'2020-11-08 17:02:20');

INSERT INTO `messages` (`id`, `type`, `from_id`, `to_id`, `text`, `created_at`) VALUES
(1,	'DANGER',	2,	3,	'Objednávka č. 20. Změna čísla na +420 553 625 412',	'2020-11-08 16:35:41'),
(2,	'INFO',	2,	3,	'Svátek 17. listopadu - Den boje za svobodu a demokracii. Nahlašte si směny na vedení.',	'2020-11-08 16:35:41');

INSERT INTO `orders` (`id`, `ean_code`, `created_at`, `contact_name`, `contact_surname`, `address_number`, `street`, `city_id`, `zip_id`, `phone_number`, `email`, `name_invoicing`, `surname_invoicing`, `street_invoicing`, `invoicing_city_id`, `invoicing_zip_id`, `delivery_date`, `terms_of_services_agreement`, `order_source`, `ic`, `dic`, `partner_branch_id`, `payment_type_id`, `delivery_type_id`) VALUES
(1,	'12365412',	'2020-11-08 16:39:34',	'Marie',	'Novotná',	'56',	'Palackého',	2,	3,	'+420774326',	'marie.novotna@seznam.cz',	'Marie',	'Novotná',	'Palackého',	2,	3,	'2020-11-09 14:00:00',	1,	'web jaso.cz',	NULL,	NULL,	3,	1,	1),
(2,	'12365412',	'2020-11-08 17:42:34',	'Petr',	'Rychlý',	'32',	'Masarykova',	2,	2,	'+420245365',	'petr.rychly@email.cz',	'Petr',	'Rychlý',	'Masarykova',	2,	2,	'2020-11-08 19:00:00',	1,	'web jaso.cz',	'58456',	'CZ45651',	2,	1,	3);

INSERT INTO `order_items` (`id`, `order_id`, `item_name`, `description`, `weight`, `resolution`, `brutto`, `netto`, `vat`, `quantity`, `created_at`, `order_item_state_id`) VALUES
(1,	1,	'1795 exportní ležák 30l KEG ( SAMSON )',	NULL,	NULL,	NULL,	984.21,	813.4,	21,	2,	'2020-11-08 16:46:52',	1),
(2,	1,	'SAMSON 10 30l KEG výč. světlé',	NULL,	30000,	'20x50x30',	888.01,	733.89,	21,	1,	'2020-11-08 16:46:52',	1),
(3,	1,	'Adamova žebra medová neostrá 500g - alu - GurmEko',	NULL,	500,	'20x50x30',	143.8,	125.05,	21,	10,	'2020-11-08 17:46:52',	1);

INSERT INTO `order_item_states` (`id`, `type`, `description`) VALUES
(1,	'OK',	''),
(2,	'DAMAGED',	''),
(3,	'NOT_TAKEN',	'');

INSERT INTO `order_logs` (`id`, `order_id`, `order_state_id`, `created_by`, `assigned_to`, `created_at`) VALUES
(1,	1,	2,	NULL,	NULL,	'2020-11-08 16:53:56'),
(2,	2,	1,	NULL,	NULL,	'2020-11-08 16:53:56'),
(3,	2,	2,	2,	6,	'2020-11-08 16:58:56'),
(4,	2,	3,	6,	NULL,	'2020-11-08 17:53:56'),
(5,	2,	11,	2,	3,	'2020-11-08 18:05:56'),
(6,	2,	4,	3,	NULL,	'2020-11-08 18:08:56'),
(7,	2,	5,	3,	NULL,	'2020-11-08 18:09:56');

INSERT INTO `order_states` (`id`, `type`, `description`) VALUES
(1,	'CREATED',	''),
(2,	'ASSIGNED_TO_BRANCH',	''),
(3,	'READY',	''),
(4,	'ACCEPTED',	''),
(5,	'DELIVERING',	''),
(6,	'DELIVERED',	''),
(7,	'COMPLETED',	''),
(8,	'ON_HOLD',	''),
(9,	'DAMAGED',	''),
(10,	'NOT_TAKEN',	''),
(11,	'ASSIGNED_TO_COURRIER',	'');

INSERT INTO `partners` (`id`, `name`, `partner_from`, `email`, `phone_number`, `ico`, `dic`, `created_at`, `admin_user_id`, `deleted`) VALUES
(1,	'JASO-DISTRIBUTOR, spol. s r.o.',	'2020-11-08 16:25:39',	'jakubcova@jaso.cz',	'+420 777 737 357',	'63477734',	'CZ63477734',	'2020-11-08 16:25:39',	4,	0);

INSERT INTO `partner_branches` (`id`, `name`, `email`, `phone_number`, `partner_id`, `address_number`, `street`, `city_id`, `zip_id`, `branch_user_id`, `created_at`, `deleted`) VALUES
(1,	'Jaso Střelice',	'strelice@jaso.cz',	'+420 544 500 120',	1,	'726',	'Areál skladů Střelice',	1,	1,	5,	'2020-11-08 16:27:39',	0),
(2,	'Jaso Palackého',	'palackeho@jasocc.cz',	'+420 541 246 252',	1,	'91',	'Palackého',	2,	2,	6,	'2020-11-08 16:27:39',	0),
(3,	'Jaso Palackého',	'palackeho@jasocc.cz',	'+420 548 130 114',	1,	'9',	'Masná',	2,	3,	7,	'2020-11-08 16:27:39',	0);

INSERT INTO `payment_types` (`id`, `type`, `description`, `active`) VALUES
(1,	'CARD',	'',	1),
(2,	'CASH',	'',	1),
(3,	'MEAL_TICKET',	'',	1);

INSERT INTO `roles_x_actions` (`role_id`, `action_id`) VALUES
(1,	2),
(1,	8),
(1,	18),
(1,	17),
(1,	4),
(1,	11),
(1,	1),
(4,	1),
(4,	3),
(5,	1),
(1,	9),
(1,	19),
(1,	21),
(1,	20),
(1,	22),
(1,	23),
(1,	24),
(1,	7),
(1,	25),
(1,	26),
(2,	9),
(2,	20),
(2,	22),
(2,	23),
(2,	24),
(1,	3),
(1,	5),
(1,	6),
(1,	10),
(1,	12),
(1,	14),
(1,	15),
(1,	16),
(2,	26),
(3,	1),
(3,	3),
(3,	9),
(3,	15),
(4,	9),
(5,	9),
(1,	27),
(6,	1),
(6,	28),
(6,	2),
(6,	3),
(6,	27),
(6,	4),
(6,	5),
(6,	6),
(6,	7),
(6,	8),
(6,	9),
(6,	19),
(6,	20),
(6,	21),
(6,	22),
(6,	23),
(6,	24),
(6,	10),
(6,	11),
(6,	12),
(6,	14),
(6,	15),
(6,	16),
(6,	17),
(6,	18),
(6,	25),
(6,	26),
(5,	28),
(5,	25),
(4,	28),
(4,	25),
(2,	1),
(2,	28),
(2,	3),
(2,	4),
(2,	7),
(2,	15),
(2,	18),
(2,	25),
(3,	26),
(3,	20),
(3,	21),
(1,	28);

INSERT INTO `users` (`id`, `email`, `password`, `name`, `surname`, `role_id`, `active`, `reset_token`, `phone_number`, `last_login`, `created_at`, `default_lang`) VALUES
(1,	'daniel.cermak@soused-online.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Daniel',	'Čermák',	1,	1,	NULL,	'777542365',	NULL,	'2020-11-08 16:11:04',	'cs'),
(2,	'radmila.novakova@soused-online.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Radmila',	'Nováková',	2,	1,	NULL,	'654823546',	NULL,	'2020-11-08 16:11:04',	'cs'),
(3,	'pavel.novotny@soused-online.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Pavel',	'Novotný',	3,	1,	NULL,	'654214563',	NULL,	'2020-11-08 16:11:04',	'cs'),
(4,	'jirina.jakubcova@jaso.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Jiřina',	'Jakubcová',	4,	1,	NULL,	'777545233',	NULL,	'2020-11-08 16:11:04',	'cs'),
(5,	'strelice@jaso.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Jaso',	'Střelice',	5,	1,	NULL,	'+420 544 500 120',	NULL,	'2020-11-08 16:11:04',	'cs'),
(6,	'palackeho@jaso.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Jaso',	'Palackého',	5,	1,	NULL,	'+420 541 246 252',	NULL,	'2020-11-08 16:11:04',	'cs'),
(7,	'masna@jaso.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Jaso',	'Masná',	5,	1,	NULL,	'+420 548 130 114',	NULL,	'2020-11-08 16:11:04',	'cs'),
(8,	'petr.sebela@yobbiware.cz',	'$2y$10$oA6Du5OAxSx1C/5mJKmqpOEDB8quZWLsS/lzJlb10NU8Hb.T.DSpe',	'Petr',	'Šebela',	6,	1,	NULL,	'+420 605 200 686',	NULL,	'2020-11-08 16:11:04',	'cs');

INSERT INTO `vehicles` (`id`, `spz`, `vehicle_brand`, `vehicle_model`, `vehicle_color`, `start_km`, `end_km`, `created_at`, `type`, `deleted`) VALUES
(2,	'4B25455',	'volkswagen',	'Caddy',	'red',	65230,	70123,	'2020-09-24 20:54:00',	'CAR',	0),
(3,	'6B51234',	'fiat',	'Doblo',	'white',	12305,	15011,	'2020-09-24 20:55:00',	'CAR',	0),
(4,	'1B45157',	'motorro',	'Digita',	'black',	2356,	3542,	'2020-09-24 20:56:54',	'SCOOTER',	0),
(5,	'1B45158',	'motorro',	'Digita',	'black',	4531,	5821,	'2020-09-24 20:56:54',	'SCOOTER',	0),
(6,	'1B45159',	'motorro',	'Digita',	'black',	3215,	4587,	'2020-09-24 20:56:54',	'SCOOTER',	0),
(7,	'1B45160',	'motorro',	'Digita',	'yellow',	10532,	10540,	'2020-09-24 20:56:54',	'SCOOTER',	0);

INSERT INTO `vehicle_logs` (`id`, `event`, `created_at`, `created_by`, `vehicle_id`) VALUES
(2,	'ASSIGN',	'2020-11-08 16:58:06',	3,	2),
(3,	'TANKING',	'2020-11-08 17:58:06',	3,	2),
(4,	'ACCIDENT',	'2020-11-08 18:30:06',	3,	2);

INSERT INTO `zips` (`id`, `zip`, `city_id`, `deleted`) VALUES
(1,	66447,	1,	0),
(2,	61200,	2,	0),
(3,	60200,	2,	0);

-- 2020-11-08 16:56:02§