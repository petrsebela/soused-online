SET NAMES utf8mb4;

INSERT INTO `break_logs` (`id`, `length`, `state`, `created_at`, `accepted_at`, `changed_by`, `requested_by`) VALUES
(1,	10,	'CREATED',	'2020-11-17 18:21:15',	NULL,	NULL,	3),
(2,	20,	'DELCINED',	'2020-11-17 18:21:31',	NULL,	1,	3),
(3,	15,	'ACCEPTED',	'2020-11-17 18:24:31',	'2020-11-17 18:30:31',	1,	3);

INSERT INTO `cash_registers` (`id`, `amount`, `user_id`, `created_at`, `payment_type_id`, `description`) VALUES
(1,	100,	3,	'2020-11-17 18:23:49',	1,	NULL),
(2,	500,	3,	'2020-11-17 18:24:29',	2,	NULL),
(3,	-1000,	3,	'2020-11-17 18:24:49',	2,	'Tankování');