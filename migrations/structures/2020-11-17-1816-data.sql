CREATE TABLE `break_logs` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `length` int NOT NULL,
  `state` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `accepted_at` datetime NULL,
  `changed_by` int(11) NULL,
  `requested_by` int(11) NOT NULL,
  FOREIGN KEY (`changed_by`) REFERENCES `users` (`id`),
  FOREIGN KEY (`requested_by`) REFERENCES `users` (`id`)
);

CREATE TABLE `cash_registers` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `amount` float NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`)
);

ALTER TABLE `cash_registers`
ADD `description` varchar(255) NULL;