CREATE TABLE `settings` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `option` varchar(100) NOT NULL,
  `value` varchar(100) NULL,
  `show_in_settings` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL
);

ALTER TABLE `order_items`
ADD `quantity` int(10) NOT NULL AFTER `vat`;