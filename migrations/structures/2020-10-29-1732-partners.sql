ALTER TABLE `partners`
ADD `deleted` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `partner_branches`
ADD `deleted` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `payment_types`
ADD `active` tinyint(1) NOT NULL DEFAULT '0';