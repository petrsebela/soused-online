ALTER TABLE `vehicle_logs`
ADD `vehicle_id` int(11) NOT NULL;
ALTER TABLE `vehicle_logs`
ADD FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`);
ALTER TABLE `vehicle_logs`
CHANGE `vehicle_id` `vehicle_id` int(11) NULL AFTER `created_by`;