ALTER TABLE `vehicle_logs`
CHANGE `created_by` `created_by` int(11) NOT NULL AFTER `created_at`,
CHANGE `vehicle_id` `vehicle_id` int(11) NOT NULL AFTER `created_by`;