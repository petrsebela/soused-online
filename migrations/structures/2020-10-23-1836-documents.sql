CREATE TABLE `documents` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(100) NULL,
  `partner_id` int(11) NULL,
  `file_path` varchar(200) NULL,
  `created_at` datetime NOT NULL,
  FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`)
) ENGINE='InnoDB';
