ALTER TABLE `api_keys`
ADD `partner_id` int(11) NULL,
ADD FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`);