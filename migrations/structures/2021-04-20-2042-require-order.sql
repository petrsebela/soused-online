ALTER TABLE `orders`
CHANGE `email` `email` varchar(30) COLLATE 'utf8mb4_general_ci' NULL AFTER `phone_number`,
CHANGE `name_invoicing` `name_invoicing` varchar(30) COLLATE 'utf8mb4_general_ci' NULL AFTER `email`,
CHANGE `surname_invoicing` `surname_invoicing` varchar(30) COLLATE 'utf8mb4_general_ci' NULL AFTER `name_invoicing`,
CHANGE `street_invoicing` `street_invoicing` varchar(30) COLLATE 'utf8mb4_general_ci' NULL AFTER `surname_invoicing`,
CHANGE `invoicing_city_id` `invoicing_city_id` int(11) NULL AFTER `street_invoicing`,
CHANGE `invoicing_zip_id` `invoicing_zip_id` int(11) NULL AFTER `invoicing_city_id`;