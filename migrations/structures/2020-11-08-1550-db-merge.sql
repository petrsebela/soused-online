DROP TABLE IF EXISTS `api_keys`;
CREATE TABLE `api_keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(32) COLLATE utf8_bin NOT NULL,
  `description` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;


ALTER TABLE `order_logs`
ADD `assigned_to` int(11) NULL AFTER `created_by`;
ALTER TABLE `order_logs`
ADD FOREIGN KEY (`assigned_to`) REFERENCES `users` (`id`);

ALTER TABLE `partners`
CHANGE `created_At` `created_at` datetime NOT NULL AFTER `dic`;

ALTER TABLE `orders`
CHANGE `ic` `ic` varchar(10) NULL AFTER `order_source`;

CREATE TABLE `messages` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` enum('INFO','WARNING','DANGER','SUCCESS') NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `created_at` datetime NOT NULL,
  FOREIGN KEY (`from_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`to_id`) REFERENCES `users` (`id`)
);

CREATE TABLE `knowledge` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `heading` varchar(40) NOT NULL,
  `description` text NULL,
  `created_at` datetime NOT NULL
);

ALTER TABLE `users`
CHANGE `email` `email` varchar(50) COLLATE 'utf8mb4_general_ci' NOT NULL AFTER `id`;

ALTER TABLE `orders`
DROP `remark`;

ALTER TABLE `order_logs`
CHANGE `created_by` `created_by` int(11) NULL AFTER `order_state_id`;

CREATE TABLE `courier_logs` (
  `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `courier_id` int(11) NOT NULL,
  FOREIGN KEY (`courier_id`) REFERENCES `users` (`id`)
);