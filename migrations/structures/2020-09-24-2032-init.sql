-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `right_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `right_id` (`right_id`),
  CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`right_id`) REFERENCES `rights` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `delivery_types`;
CREATE TABLE `delivery_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ean_code` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `contact_name` varchar(30) NOT NULL,
  `contact_surname` varchar(30) NOT NULL,
  `address_number` varchar(30) NOT NULL,
  `street` varchar(30) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip_id` int(11) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `name_invoicing` varchar(30) NOT NULL,
  `surname_invoicing` varchar(30) NOT NULL,
  `street_invoicing` varchar(30) NOT NULL,
  `invoicing_city_id` int(11) NOT NULL,
  `invoicing_zip_id` int(11) NOT NULL,
  `delivery_date` datetime NOT NULL,
  `terms_of_services_agreement` tinyint(1) NOT NULL,
  `remark` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`remark`)),
  `order_source` varchar(20) NOT NULL,
  `ic` varbinary(10) DEFAULT NULL,
  `dic` varchar(10) DEFAULT NULL,
  `partner_branch_id` int(11) NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `delivery_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `phone_number` (`phone_number`),
  KEY `email` (`email`),
  KEY `name_invoicing` (`name_invoicing`),
  KEY `surname_invoicing` (`surname_invoicing`),
  KEY `street_invoicing` (`street_invoicing`),
  KEY `partner_branch_id` (`partner_branch_id`),
  KEY `city_id` (`city_id`),
  KEY `zip_id` (`zip_id`),
  KEY `invoicing_city_id` (`invoicing_city_id`),
  KEY `invoicing_zip_id` (`invoicing_zip_id`),
  KEY `payment_type_id` (`payment_type_id`),
  KEY `delivery_type_id` (`delivery_type_id`),
  CONSTRAINT `orders_ibfk_10` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `orders_ibfk_11` FOREIGN KEY (`zip_id`) REFERENCES `zips` (`id`),
  CONSTRAINT `orders_ibfk_12` FOREIGN KEY (`invoicing_city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `orders_ibfk_13` FOREIGN KEY (`invoicing_zip_id`) REFERENCES `zips` (`id`),
  CONSTRAINT `orders_ibfk_14` FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`),
  CONSTRAINT `orders_ibfk_15` FOREIGN KEY (`delivery_type_id`) REFERENCES `delivery_types` (`id`),
  CONSTRAINT `orders_ibfk_9` FOREIGN KEY (`partner_branch_id`) REFERENCES `partner_branches` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `order_items`;
CREATE TABLE `order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_name` varchar(100) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `resolution` varchar(200) DEFAULT NULL,
  `brutto` float NOT NULL,
  `netto` float NOT NULL,
  `vat` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `order_item_state_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `order_item_state_id` (`order_item_state_id`),
  CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`order_item_state_id`) REFERENCES `order_item_states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `order_item_states`;
CREATE TABLE `order_item_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `order_logs`;
CREATE TABLE `order_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `order_state_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `created_by` (`created_by`),
  KEY `order_state_id` (`order_state_id`),
  CONSTRAINT `order_logs_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_logs_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  CONSTRAINT `order_logs_ibfk_3` FOREIGN KEY (`order_state_id`) REFERENCES `order_states` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `order_states`;
CREATE TABLE `order_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `partner_from` datetime NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `ico` varchar(10) DEFAULT NULL,
  `dic` varchar(10) DEFAULT NULL,
  `created_At` datetime NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `admin_user_id` (`admin_user_id`),
  CONSTRAINT `partners_ibfk_1` FOREIGN KEY (`admin_user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `partner_branches`;
CREATE TABLE `partner_branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone_number` varchar(50) DEFAULT NULL,
  `partner_id` int(11) NOT NULL,
  `address_number` varchar(50) NOT NULL,
  `street` varchar(50) NOT NULL,
  `city_id` int(11) NOT NULL,
  `zip_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `partner_id` (`partner_id`),
  KEY `city_id` (`city_id`),
  KEY `zip_id` (`zip_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `partner_branches_ibfk_1` FOREIGN KEY (`partner_id`) REFERENCES `partners` (`id`),
  CONSTRAINT `partner_branches_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `partner_branches_ibfk_3` FOREIGN KEY (`zip_id`) REFERENCES `zips` (`id`),
  CONSTRAINT `partner_branches_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `payment_types`;
CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `int_name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `roles_x_actions`;
CREATE TABLE `roles_x_actions` (
  `role_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  KEY `role_id` (`role_id`),
  KEY `action_id` (`action_id`),
  CONSTRAINT `roles_x_actions_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `roles_x_actions_ibfk_2` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(20) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `name` varchar(20) NOT NULL,
  `surname` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `reset_token` varchar(30) DEFAULT NULL,
  `phone_number` varchar(20) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `default_lang` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spz` varchar(10) NOT NULL,
  `vehicle_brand` varchar(10) NOT NULL,
  `vehicle_model` varchar(10) NOT NULL,
  `vehicle_color` varchar(10) NOT NULL,
  `start_km` int(20) NOT NULL,
  `end_km` int(20) NOT NULL,
  `created_at` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `vehicle_logs`;
CREATE TABLE `vehicle_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spz` varchar(10) NOT NULL,
  `event` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by` (`created_by`),
  CONSTRAINT `vehicle_logs_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `zips`;
CREATE TABLE `zips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zip` int(10) NOT NULL,
  `city_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_id` (`city_id`),
  CONSTRAINT `zips_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- 2020-09-24 18:33:12

ALTER TABLE `vehicles`
ADD `deleted` tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE `vehicles`
CHANGE `created_at` `created_at` datetime NOT NULL AFTER `end_km`;

ALTER TABLE `vehicles`
ADD `type` varchar(30) NOT NULL AFTER `created_at`;